/* @flow */

import React from 'react';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

type TimeValue = { hh: number, mm: number, ss?: number }
type Props = {
  visible: boolean,
  timeValue: TimeValue,
  onChange: (selectedValue: Date) => void,
  closePicker: Function
}

const ANDROIDTimePicker = (props: Props) => {
  const { hh, mm, ss } = props.timeValue;
  const value = moment().hour(hh)
    .minute(mm)
    .second(ss)
    .millisecond(0);

  const { visible } = props;

  if (!visible) return null;

  const onChange = (e, selectedValue: Date) => {
    if (!selectedValue) {
      props.closePicker();
    } else {
      props.onChange(selectedValue);
    }
  };

  return (
    <DateTimePicker
      display={'default'}
      value={value.toDate()}
      is24Hour
      mode={'time'}
      onChange={onChange}
    />
  );
};

export default React.forwardRef<Props, Object>(ANDROIDTimePicker);
