/* @flow */

import React from 'react';
import { StyleSheet } from 'react-native';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import Modal from '../Modal';

type TimeValue = { hh: number, mm: number, ss?: number }
type Props = {
  visible: boolean,
  timeValue: TimeValue,
  onChange: (selectedValue: Date) => void,
  iOSReturnButtonText: string,
  closePicker: Function
}

const IOSTimePicker = (props: Props) => {
  const {
    timeValue, iOSReturnButtonText, onChange, closePicker,
  } = props;

  const { hh, mm, ss = 0 } = timeValue;
  const value = moment()
    .hour(hh)
    .minute(mm)
    .second(ss)
    .millisecond(0);

  return (
    <Modal
      visible={props.visible}
      onClose={closePicker}
      returnButtonText={iOSReturnButtonText}
    >
      <DateTimePicker
        display={'default'}
        value={value.toDate()}
        is24Hour
        mode={'time'}
        onChange={(e, selectedValue: Date) => onChange(selectedValue)}
        style={styles.backgroundColor}
      />
    </Modal>
  );
};

const bgWhite = '#FFFFFF';
const styles = StyleSheet.create({
  backgroundColor: { backgroundColor: bgWhite },
});

export default React.forwardRef<Props, Object>(IOSTimePicker);
