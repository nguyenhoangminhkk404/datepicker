/* @flow */

import React from 'react';
import { Platform } from 'react-native';
import moment from 'moment';
import Picker from './Picker';

type TimeValue = { hh: number, mm: number, ss?: number }
type Props = {
  timeValue: TimeValue,
  onSelected: (selectedValue: TimeValue) => void,
  returnButtonText: string,
}

const TimePicker = (props: Props, ref: Object) => {
  const { timeValue, onSelected, returnButtonText } = props;
  const [pickerShown, setPickerShown] = React.useState<boolean>(false);

  React.useImperativeHandle(ref, () => ({
    showPicker: () => setPickerShown(true),
  }));

  const onChange = (selectedTime: Date) => {
    if (Platform.OS === 'android') setPickerShown(false);
    // android picker cancel press will return selectedTime as undefine.
    if (moment(selectedTime).isValid()) {
      const timeFormated = {
        hh: moment(selectedTime).hour(),
        mm: moment(selectedTime).minute(),
        ss: moment(selectedTime).second(),
      };
      onSelected(timeFormated);
    }
  };

  return (
    <Picker
      visible={pickerShown}
      timeValue={timeValue}
      onChange={onChange}
      iOSReturnButtonText={returnButtonText}
      closePicker={() => setPickerShown(false)}
    />
  );
};

TimePicker.defaultProps = {
  returnButtonText: '',
};

export default React.forwardRef<Props, Object>(TimePicker);
