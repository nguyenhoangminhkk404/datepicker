/* @flow */

import React from 'react';
import { StyleSheet } from 'react-native';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import Modal from '../Modal';

type DateValue = { dd: number, mm: number, yy: number } // mm: 0-11
type Props = {
  visible: boolean,
  dateValue: DateValue,
  minimumDate?: Date,
  maximumDate?: Date,
  onChange: (selectedValue: Date) => void,
  iOSReturnButtonText: string,
  closePicker: Function
}

const IOSDatePicker = (props: Props) => {
  const {
    minimumDate, maximumDate, onChange, dateValue,
    iOSReturnButtonText, closePicker,
  } = props;

  const { dd, mm, yy } = dateValue;
  const value = moment([yy, mm, dd]).toDate();
  const minimum = moment(minimumDate).isValid() ? { minimumDate } : {};
  const maximum = moment(maximumDate).isValid() ? { maximumDate } : {};

  return (
    <Modal
      visible={props.visible}
      onClose={closePicker}
      returnButtonText={iOSReturnButtonText}
    >
      <DateTimePicker
        display={'default'}
        value={value}
        mode={'date'}
        onChange={(e, selectedValue: Date) => onChange(selectedValue)}
        /* eslint-disable react/jsx-props-no-spreading */
        {...minimum}
        {...maximum}
        style={styles.backgroundColor}
      />
    </Modal>
  );
};

const bgWhite = '#FFFFFF';
const styles = StyleSheet.create({
  backgroundColor: { backgroundColor: bgWhite },
});

export default React.forwardRef<Props, Object>(IOSDatePicker);
