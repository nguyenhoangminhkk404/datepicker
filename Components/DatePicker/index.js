/* @flow */

import React from 'react';
import { Platform } from 'react-native';
import moment from 'moment';
import Picker from './Picker';

type DateValue = { dd: number, mm: number, yy: number } // mm: 0-11
type Props = {
  dateValue: DateValue,
  minimumDate?: Date,
  maximumDate?: Date,
  onSelected: (selectedValue: DateValue) => void,
  returnButtonText: string,
}

const DatePicker = (props: Props, ref: Object) => {
  const {
    dateValue, minimumDate, maximumDate,
    onSelected, returnButtonText,
  } = props;
  const [pickerShown, setPickerShown] = React.useState<boolean>(false);

  React.useImperativeHandle(ref, () => ({
    showPicker: () => setPickerShown(true),
  }));


  const onChange = (selectedDate: Date) => {
    if (Platform.OS === 'android') setPickerShown(false);
    // android picker cancel press will return selectedDate as undefine.
    if (moment(selectedDate).isValid()) {
      const dateFormated = {
        dd: moment(selectedDate).get('date'),
        mm: moment(selectedDate).get('month'),
        yy: moment(selectedDate).get('year'),
      };
      onSelected(dateFormated);
    }
  };

  return (
    <Picker
      visible={pickerShown}
      dateValue={dateValue}
      minimumDate={minimumDate}
      maximumDate={maximumDate}
      onChange={onChange}
      iOSReturnButtonText={returnButtonText}
      closePicker={() => setPickerShown(false)}
    />
  );
};

DatePicker.defaultProps = {
  returnButtonText: '',
};

export default React.forwardRef<Props, Object>(DatePicker);
