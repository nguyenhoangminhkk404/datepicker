/* @flow */

import React from 'react';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

type DateValue = { dd: number, mm: number, yy: number } // mm: 0-11
type Props = {
  visible: boolean,
  dateValue: DateValue,
  minimumDate?: Date,
  maximumDate?: Date,
  onChange: (selectedValue: Date) => void,
  closePicker: Function
}

const ANDROIDDatePicker = (props: Props) => {
  const {
    minimumDate, maximumDate, dateValue, visible,
  } = props;

  if (!visible) return null;

  const { dd, mm, yy } = dateValue;
  const value = moment([yy, mm, dd]).toDate();
  const minimum = moment(minimumDate).isValid() ? { minimumDate } : {};
  const maximum = moment(maximumDate).isValid() ? { maximumDate } : {};

  const onChange = (e, selectedValue: Date) => {
    if (!selectedValue) props.closePicker();
    else props.onChange(selectedValue);
  };

  return (
    <DateTimePicker
      display={'default'}
      value={value}
      mode={'date'}
      onChange={onChange}
      /* eslint-disable react/jsx-props-no-spreading */
      {...minimum}
      {...maximum}
    />
  );
};

export default React.forwardRef<Props, Object>(ANDROIDDatePicker);
