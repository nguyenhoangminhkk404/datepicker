/* @flow */

import React from 'react';
import {
  StyleSheet, Animated, TouchableOpacity, Dimensions,
  Modal, Text, View, TouchableWithoutFeedback,
} from 'react-native';

type Props = {
  children: React$Node,
  visible: boolean,
  onClose: Function,
  returnButtonText: string,
  childrenHeight: number
};

const OPACITY_ANIM_DURATION = 250;
const CONFIRM_BAR_HEIGHT = 40;
const TRANSPARENT_ANIM_DURATION = 250;

const CustomModal = (props: Props) => {
  const {
    visible, onClose, returnButtonText, children, childrenHeight,
  } = props;
  const [currentVisible, setVisible] = React.useState(visible);
  const modalAnim = React.useRef(new Animated.Value(0)).current;
  const childrenAnim = React.useRef(new Animated.Value(childrenHeight)).current;

  React.useEffect(() => {
    if (visible) setVisible(true);
    else if (!visible) {
      Animated.parallel([
        Animated.timing(childrenAnim, {
          toValue: childrenHeight,
          duration: TRANSPARENT_ANIM_DURATION,
          useNativeDriver: true,
        }),
        Animated.timing(modalAnim, {
          toValue: 0,
          duration: OPACITY_ANIM_DURATION,
          useNativeDriver: true,
        }),
      ]).start(() => {
        setVisible(false);
        onClose();
      });
    }
  }, [visible]);

  React.useEffect(() => {
    if (currentVisible) {
      Animated.parallel([
        Animated.timing(modalAnim, {
          toValue: 1,
          duration: OPACITY_ANIM_DURATION,
          useNativeDriver: true,
        }),
        Animated.timing(childrenAnim, {
          toValue: 0,
          duration: TRANSPARENT_ANIM_DURATION,
          useNativeDriver: true,
        }),
      ]).start();
    }
  }, [currentVisible]);

  if (!currentVisible) return null;

  return (
    <Modal visible animationType={'none'} transparent>
      <TouchableWithoutFeedback onPress={onClose}>
        <Animated.View style={[styles.container, { opacity: modalAnim }]} />
      </TouchableWithoutFeedback>
      <Animated.View style={[
        styles.childrenPositionView,
        {
          opacity: modalAnim,
          transform: [{ translateY: childrenAnim }],
        },
      ]}
      >
        {
          returnButtonText ? (
            <View style={styles.confirmBar}>
              <TouchableOpacity
                style={styles.confirmButton}
                onPress={onClose}
              >
                <Text>{returnButtonText}</Text>
              </TouchableOpacity>
            </View>
          ) : null
        }
        {children}
      </Animated.View>
    </Modal>
  );
};

const bgGray = '#E8ECEF';
const overlayColor = 'rgba(40,50,65,0.6)';
const { height: screenHeight, width: screenWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  childrenPositionView: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth,
  },
  confirmBar: {
    backgroundColor: bgGray,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingHorizontal: 15,
    height: CONFIRM_BAR_HEIGHT,
  },
  confirmButton: { padding: 5 },
  container: { flex: 1, backgroundColor: overlayColor },
});

CustomModal.defaultProps = {
  returnButtonText: '',
  childrenHeight: screenHeight,
};

export default CustomModal;
