module.exports = {
  root: true,
  parser: 'babel-eslint',
  plugins: ['react-native', 'react-hooks', 'flowtype'],
  extends: [
    'airbnb',
    'plugin:react-native/all',
    'plugin:flowtype/recommended',
  ],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      'jsx': true
    }
  },
  env: {
    'es6': true,
    'node': true,
    'jasmine': true,
    'react-native/react-native': true
  },
  rules: {
    'default-case': 0,
    'no-nested-ternary': 0,
    'no-use-before-define': 0,
    'lines-between-class-members': ['error', 'always', { 'exceptAfterSingleLine': true }],
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default-member': 0,
    'react/no-unused-prop-types': 1,
    'react/destructuring-assignment': 0,
    'react/prefer-stateless-function': 0,
    'react/require-default-props': 0,
    'react/state-in-constructor': 0,
    'react/jsx-curly-brace-presence': 0,
    'react/jsx-filename-extension': [2, { 'extensions': ['.js', '.jsx'] }],
    'react/default-props-match-prop-types': [2, { 'allowRequiredDefaults': true }],
    'react-native/no-raw-text': 0,
    'react-native/no-unused-styles': 2,
    'react-native/no-inline-styles': 2,
    'react-native/no-color-literals': 2,
    'react-native/split-platform-components': 2,
    'react-native/sort-styles': ['error', 'asc', { 'ignoreStyleProperties': true }],
    'react/static-property-placement': 1
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true
    },
    'import/resolver': {
      node: {
        extensions: [
          '.js',
          '.android.js',
          '.ios.js'
        ]
      }
    }
  },
  globals: {
    __DEV__: true
  }
};
