/* @flow */

import React from 'react';
import {
  StyleSheet, View, Button, Text,
} from 'react-native';
import moment from 'moment';
import { DatePicker, TimePicker } from './Components';

const App: () => React$Node = () => {
  const [dateValue, setDateValue] = React.useState<Date>(moment().toDate());
  const [timeValue, setTimeValue] = React.useState<Date>(moment().toDate());
  const datePickerRef: Object = React.useRef();
  const timePickerRef: Object = React.useRef();

  const onDateSelected = ({ dd, mm, yy }: Object) => {
    const momentConverted = moment([yy, mm, dd]);
    if (momentConverted.isValid()) {
      setDateValue(momentConverted.toDate());
    }
  };

  const onTimeSelected = ({ hh, mm, ss = 0 }: Object) => {
    setTimeValue(
      moment()
        .hour(hh)
        .minute(mm)
        .second(ss)
        .millisecond(0)
        .toDate(),
    );
  };

  const inputDate = {
    dd: moment(dateValue).date(),
    mm: moment(dateValue).month(),
    yy: moment(dateValue).year(),
  };

  const inputTime = {
    hh: moment(timeValue).hour(),
    mm: moment(timeValue).minute(),
    ss: moment(timeValue).second(),
  };

  return (
    <View style={styles.container}>
      <Button
        title={'date picker'}
        onPress={() => datePickerRef.current.showPicker()}
      />
      <Button
        title={'time picker'}
        onPress={() => timePickerRef.current.showPicker()}
      />

      <Text>{`dateValue: ${moment(dateValue).format('DD/MM/YYYY')}`}</Text>
      <Text>{`timeValue: ${moment(timeValue).format('hh:mm:ss A')}`}</Text>

      <DatePicker
        ref={datePickerRef}
        returnButtonText={'Done1'}
        dateValue={inputDate}
        onSelected={onDateSelected}
      />

      <TimePicker
        ref={timePickerRef}
        returnButtonText={'Done2'}
        timeValue={inputTime}
        onSelected={onTimeSelected}
      />

    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, marginTop: 100 },
});

export default App;
